const cv = require("opencv");
const serviceConfiguration = require("./configuration");
const singleImageTransforms = require("./singleImageTransforms.js");
const imageListProcessors = require("./imageListAggregators.js");

const {pctOrValue} = require("./imageUtils.js");

var subIntervalCounters = {};

function resetInternalState(){
	subIntervalCounters = {};
}

function croppedImage(image, cropConfig){
	if (cropConfig){
		var x = pctOrValue(cropConfig.x || 0, image.width());
		var y =  pctOrValue(cropConfig.y || 0, image.height());
		var width = Math.min(pctOrValue(cropConfig.w || "100%", image.width()), image.width() - x);
		var height = Math.min(pctOrValue(cropConfig.h || "100%",  image.height()), image.height() - y);
		return image.crop(x,y, width, height);
	}
	return image;
}

function imageDataToString(data){
	return ((data instanceof Buffer) ? data : data.toBuffer()).toString("base64");
}

async function getMetadataForImage(image, stepConfig, collectionInterval){
	var startTime = new Date();
	var size = 0;
	image = croppedImage(image, stepConfig.parameters && stepConfig.parameters.crop);
	if (image){
		size = image.size();
	}

	var transform = (stepConfig && stepConfig.imageTransform && singleImageTransforms[stepConfig.imageTransform]) ? stepConfig.imageTransform : "baseMetadata";
	var transformResult = await singleImageTransforms[transform](image, stepConfig.parameters || {});
	var endTime = new Date();
	
	return {
		metadata: {
			name: stepConfig.name,
			"function": transform,
			functionVersion: serviceConfiguration.codeVersion,
			configVersion: serviceConfiguration.configVersion,
			collectionInterval: collectionInterval, 
			inputParameters: stepConfig.parameters,
			startTime: startTime,
			endTime: endTime,
			output: transformResult && transformResult.metadata
		},
		images: ((transformResult && transformResult.images) || []).map( image => {
			return {
				name: image.name,
				stepName: stepConfig.name,
				data: imageDataToString(image.data)
			}
		})
	};
}

async function getMetadataForImageList(imageList, stepConfig, duration, collectionInterval){
	if (!imageList.length){
		console.log("no images supplied for processing.");
		return {};
	}

	if (stepConfig.parameters && stepConfig.parameters.crop){
		//crop everything first
		imageList = imageList.map(image => {
			return croppedImage(image, stepConfig.parameters && stepConfig.parameters.crop);
		});
	}

	if( imageListProcessors[stepConfig.imageAggregate] ){
		var startTime = new Date();

		var output = await imageListProcessors[stepConfig.imageAggregate](imageList, stepConfig.parameters, duration);
		if (!output){
			output = {};
		}

		var imageTransform = stepConfig && stepConfig.imageTransform && singleImageTransforms[stepConfig.imageTransform];
		if(imageTransform && output && output.images && output.images.length){
			//apply the transform to every image in the list, but keep the original name
			var transformResults = (await Promise.all(output.images.map( imageInfo => {
				var originalName = imageInfo.name;
				//note: we can only feed opencv images to the single image transform, so otherwise we pass it through untransformed
				if (imageInfo.data instanceof cv.Matrix){
					var result = imageTransform(imageInfo.data, stepConfig.parameters || {});
					return {
						metadata: result && result.metadata,
						images: result && result.images && result.images.length && [{
							name:originalName,
							data: result.images[0].data
						}]
					};
				} else {
					return {
						images: [imageInfo]
					}
				}
				
			}))).filter( result => result.metadata || (result.images && result.images.length));

			//keep the metadata from the first image transformed
			if(transformResults && output && transformResults.length&& transformResults[0].metadata){
				if(!output.metadata){
					output.metadata = {};
				}
				Object.assign(output.metadata, transformResults[0].metadata);
			}

			if(transformResults && transformResults.length){
				//replace the original images with the transformed ones
				output.images = transformResults.map(result => result.images && result.images.length && result.images[0]).filter(imageInfo => imageInfo);
			} else {
				output.images = [];
			}
		}

		var endTime = new Date();
		return {
			metadata: {
				name: stepConfig.name,
				"function": imageTransform ? stepConfig.imageTransform : stepConfig.imageAggregate,
				functionVersion: serviceConfiguration.codeVersion,
				configVersion: serviceConfiguration.configVersion,
				collectionInterval: collectionInterval,
				inputParameters: stepConfig.parameters,
				startTime: startTime,
				endTime: endTime,
				output: output.metadata
			},
			images: (output.images || []).map( image => {
				return {
					name: image.name,
					stepName: stepConfig.name,
					data: imageDataToString(image.data)
				}
			})
		}
	}
}

async function transformImageToMessage(image, cameraConfiguration, date, activeRegionInfo=null){
	var stepsToRun = (cameraConfiguration.steps || []).filter(stepConfig => {
		if(stepConfig.subInterval){
			var triggerNow = !subIntervalCounters[stepConfig.name];
			subIntervalCounters[stepConfig.name] = ((subIntervalCounters[stepConfig.name] || 0) + 1) % stepConfig.subInterval;
			return triggerNow;
		}
		return true;
	});
	var stepOutputs = [];
	for (var index in stepsToRun){
		let stepConfig = stepsToRun[index];
		let collectionInterval = cameraConfiguration.interval ? (stepConfig.subInterval || 1) * cameraConfiguration.interval * 1000 : undefined;
		let stepResult = await getMetadataForImage((index==stepsToRun.length-1)? image : image.clone(), stepConfig, collectionInterval);
		stepOutputs.push(stepResult || {});
	}

	let resultList = stepOutputs.filter( stepOutput => {
		return (stepOutput.metadata && stepOutput.metadata.output) || (stepOutput.images && stepOutput.images.length);
	});

	var message = {
		type: "single",
		gatewayId: serviceConfiguration.gatewayId,
		cameraId: cameraConfiguration.id,
		collectionTime: date,
		steps: resultList.map(step => step.metadata),
		images: resultList.reduce((imageList, step) => {
			imageList.push(...step.images);
			return imageList;
		}, [])
	};

	if (activeRegionInfo!=null) {
		message.activeRegionInfo = activeRegionInfo
	}

	return message;
}

async function transformImageListToMessage(imageList, cameraConfiguration, date){
	var stepsToRun = (cameraConfiguration.steps || []).filter(stepConfig => {
		if(stepConfig.subInterval){
			var triggerNow = !subIntervalCounters[stepConfig.name];
			subIntervalCounters[stepConfig.name] = ((subIntervalCounters[stepConfig.name] || 0) + 1) % stepConfig.subInterval;
			return triggerNow;
		}
		return true;
	});
	var stepOutputs = [];
	for (var index in stepsToRun){
		let stepConfig = stepsToRun[index];
		let workingImageList = (index==stepsToRun.length-1)? imageList : imageList.map( image => image.clone() );
		let collectionInterval = cameraConfiguration.interval ? (stepConfig.subInterval || 1) * cameraConfiguration.interval * 1000 : undefined;
		let stepResult = await getMetadataForImageList(workingImageList, stepConfig, cameraConfiguration.duration, collectionInterval);
		stepOutputs.push(stepResult || {});
	}

	let resultList = stepOutputs.filter( stepOutput => {
		return (stepOutput.metadata && stepOutput.metadata.output) || (stepOutput.images && stepOutput.images.length);
	});

	var message = {
		type: "burst",
		duration: cameraConfiguration.duration,
		fps: cameraConfiguration.fps,
		gatewayId: serviceConfiguration.gatewayId,
		cameraId: cameraConfiguration.id,
		collectionTime: date,
		steps: resultList.map(step => step.metadata),
		images: resultList.reduce((imageList, step) => {
			imageList.push(...step.images);
			return imageList;
		}, [])
	};

	return message;
}

function getErrorMessage(cameraConfiguration, date, errorMessage){
	var message = {
		type: cameraConfiguration.mode,
		duration: (cameraConfiguration.mode=='burst') ? cameraConfiguration.duration : undefined,
		fps: (cameraConfiguration.mode=='burst') ? cameraConfiguration.fps : undefined,
		gatewayId: serviceConfiguration.gatewayId,
		cameraId: cameraConfiguration.id,
		collectionTime: date.getTime(),
		steps: [{
			name: 'error',
			"function": 'error',
			functionVersion: serviceConfiguration.codeVersion,
			configVersion: serviceConfiguration.configVersion,
			collectionInterval: null,
			inputParameters: null,
			startTime: date,
			endTime: new Date(),
			output: {message: errorMessage}
		}],
		images: []
	};
	return message;
}

module.exports = {
	_resetInternalState: resetInternalState,
	_getMetadataForImage:getMetadataForImage,
	_getMetadataForImageList:getMetadataForImageList,
	transformImageToMessage: transformImageToMessage,
	transformImageListToMessage:transformImageListToMessage,
	getErrorMessage: getErrorMessage
}
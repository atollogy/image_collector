
To run "npm install" locally at a Macbook with this repo, 
please run the following steps: 
```
brew install opencv
brew reinstall pkg-config
brew link opencv
```

( @tanmay found the above solution through this link: 
https://github.com/peterbraden/node-opencv/issues/441 ) 


const fs = require("fs-extra");
const moment = require("moment");
const {pctOrValue, rgbToHsv, getWhite, lightState, hueToColorName, imageSubDimensionsWithRadius, annotateAndonImage} = require("./imageUtils.js");

//This is the default transform. Returns metadata with width, height, and the image content.
module.exports.baseMetadata = function(image){
	var size = image.size();

	return {
		metadata: {
			type: "frame",
			width: size[1],
			height: size[0]
		},
		images: [{
			name: "image.jpg",
			data: image
		}]
	};
}

module.exports.averageRGB = function(image, stepConfig){
	var means = image.mean();
	var [blue, green, red] = means;

	return {
		metadata: {
			r: red, 
			g: green, 
			b: blue
		}
	};
}

var lastLogMoment = moment().startOf("second");
var logIndex = 0;
module.exports.log = function(image, stepConfig){
	fs.ensureDirSync("./log");
	var now = moment();
	var nowSecondStart = now.startOf("second");
	if(nowSecondStart.isAfter(lastLogMoment)){
		lastLogMoment = nowSecondStart;
		logIndex = 0;
	} else {
		logIndex += 1;
	}

	var fileName = (stepConfig && stepConfig.name) ? stepConfig.name : "log";

	image.save("./log/" + now.format("YYYY-MM-DD-HH-mm-ss") +"-" + fileName + "-" + logIndex + ".jpg");

	return {};
}

module.exports.markupStepCrops = function(image, stepConfig){
	var green = [0, 255, 0]; // green
	var blue = [255, 0, 0]; //blue
	var thickness = 1;
	var width = image.width();
	var height = image.height();

	//draw a green box for each crop
	stepConfig.steps.filter( step => step.parameters && step.parameters.crop).forEach((step, index) => {
		if(step.parameters.crop){
			var cropConfig = {
				x: pctOrValue(step.parameters.crop.x, width),
				y: pctOrValue(step.parameters.crop.y, height),
				w: pctOrValue(step.parameters.crop.w, width),
				h: pctOrValue(step.parameters.crop.h, height)
			};
			image.rectangle([cropConfig.x, cropConfig.y], [cropConfig.w, cropConfig.h], green, thickness);
			image.putText(step.name || (""+index), cropConfig.x + 2, cropConfig.y + 13, "HERSEY_SIMPLEX", green, 0.5, 1);
		}
	});

	//draw a blue box for location/radius specifications
	stepConfig.steps.filter( step => step.parameters && step.parameters.location).forEach((step, index) => {
		var imageBounds = {x:0, y:0, w:width, h: height};

		if(step.parameters.crop){
			imageBounds = {
				x: pctOrValue(step.parameters.crop.x, width),
				y: pctOrValue(step.parameters.crop.y, height),
				w: pctOrValue(step.parameters.crop.w, width),
				h: pctOrValue(step.parameters.crop.h, height)
			};
		}

		var centerXOffset = imageBounds.x + pctOrValue(step.parameters.location.x || 0, imageBounds.w);
		var centerYOffset = imageBounds.y + pctOrValue(step.parameters.location.y || 0, imageBounds.h);
		var radius = pctOrValue(step.parameters.radius || 10, Math.min(width, height));

		var boxX = Math.max(centerXOffset - radius, 0);
		var boxY = Math.max(centerYOffset - radius, 0);

		var targetBox = {
			x: boxX,
			y: boxY,
			w: Math.min(2 * radius, width - boxX),
			h: Math.min(2 * radius, height - boxY)
		};

		image.rectangle([targetBox.x, targetBox.y], [targetBox.w, targetBox.h], blue, thickness);
	});

	return {
		images: [{
			name: "markup.jpg",
			data: image
		}]
	}
}

module.exports.thumbnail = function(image, stepConfig){
	var width = image.width();
	var height = image.height();


	var scale = stepConfig.scale || 0.1;

	var newWidth = Math.max(Math.round(width * scale), 1);
	var newHeight = Math.max(Math.round(height * scale), 1);

	image.resize(newWidth, newHeight);

	return {
		images: [{
			name: "thumbnail.jpg",
			data: image
		}]
	}
}

// determine the state and color of light at a particular location (from a single image only)
// parameters:
//   location: {x,y}
//   radius: Radius of the detection area. Defaults to 10
//   vThreshold: Value threshold for on vs off. Defaults to 0.73
module.exports.andonState = function(image, parameters){
	var vThreshold = parameters.vThreshold || 0.73;
	var dimensions = imageSubDimensionsWithRadius(image, parameters);
	var cropped = image.crop(dimensions.x, dimensions.y, dimensions.width, dimensions.height);
	var channelMeans = cropped.mean(); 
	var [blue, green, red] = channelMeans;
	var hsvColor = rgbToHsv(red, green, blue);

	var state = lightState(hsvColor.v > vThreshold);
	var isWhite = getWhite(red, green, blue);
	var colorName = hueToColorName(hsvColor.h, isWhite);

	return {
		metadata: {
			state: state,
			color: (state == lightState(false)) ? undefined : colorName,
			hsv: hsvColor
		},
		images: [{
			name: "annotated.jpg",
			data: annotateAndonImage(image, parameters, {state:state, color: colorName}) //draw the annotated image
		}]
	};
}
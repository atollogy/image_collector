var cv = require("opencv");
var os = require("os");
var fs = require("fs");

const { execSync } = require('child_process');
var crypto = require('crypto');
var https = require("https");

var WebSocket = require('ws');
var pem = require('pem');
var auth = require('basic-auth');

var mqtt = require('mqtt');
var serviceConfiguration = require("./configuration");
const TRIGGER_TOPIC = `atollogy/imageRequest`;
const PUBLISH_TOPIC = `atollogy/imageBundle`;
var mqtt_client = mqtt.connect(serviceConfiguration.mqttServer, serviceConfiguration.mqttPort);

const CameraSource = require("./cameraSource");
const imageProcessors = require("./imageProcessors");

const singleImageTransforms = require("./singleImageTransforms");

var LISTEN_PORT = 8080;
var passwordToken = crypto.randomBytes(40).toString('base64');
var indexPage = fs.readFileSync(__dirname + "/index.html", 'utf8').replace("##passwordToken", "'"+passwordToken+"'");

var pyImage = null;

// Directory where motion_detection.py stores the images.
const MOTION_FILES_DIR = "/var/lib/atollogy/ae_camera/motion_out/";

// BASE Directory for all the sub-backlog directories
const MOTION_FILE_BACKLOG_ROOT_DIR = "/var/lib/atollogy/ae_camera/";

//Sub backlog directories name prefix
const WORK_SUB_DIR_NAME_PREFIX = "motion_out_work_";

// Regex pattern for Sub_dir check
const WORK_SUB_DIR_NAME_REGEX = "^" + WORK_SUB_DIR_NAME_PREFIX  +"[\\d]+"

/*
 * Whenever "setTimeout()" triggers "publishAEMotionImages()",
 * it will move "*.jpg" files from "/var/lib/atollogy/ae_camera/motion_out/"
 * to "/var/lib/atollogy/ae_camera/motion_out_work_(TimeStamp)/"
 *
 * At the beginning of process startup, it will check any "*.jpg" under any
 * directories /"var/lib/atollogy/ae_camera/motion_out_work_.../".
 * Move them back to  "/var/lib/atollogy/ae_camera/motion_out/",
 * before calling "setTimeout()" for "publishAEMotionImages()".
 */

const DEFAULT_BURST_DURATION = 3;
const DEFAULT_BURST_FPS = 10;

var cameraConfiguration = serviceConfiguration.cameraConfiguration;

var lastImageData;

var camera = new CameraSource(cameraConfiguration.id, {
	mode: cameraConfiguration.mode,
	orientation: cameraConfiguration.orientation || 0,
	resolution: cameraConfiguration.resolution,
	fps: cameraConfiguration.fps,
	exposureMode: cameraConfiguration.exposureMode,
	exposureTime: cameraConfiguration.exposureTime,
	whiteBalance: cameraConfiguration.whiteBalance,
	iso: cameraConfiguration.iso,
	framerate: cameraConfiguration.framerate,
	brightness: cameraConfiguration.brightness,

	optimalBrightness: cameraConfiguration.optimalBrightness,
	captureGap: cameraConfiguration.captureGap,
	captureCount: cameraConfiguration.captureCount,
	threshold: cameraConfiguration.threshold,
	regions: cameraConfiguration.regions,
	heartbeatImages: false
});

function getDirectories(path) {
	return fs.readdirSync(path).filter(function (file) {
		return fs.statSync(path+'/'+file).isDirectory();
});
}

function moveMotionFilesToTargetDir(baseBacklogDir, targetDir) {
	if(fs.existsSync(baseBacklogDir)) {
		let subDirs = getDirectories(baseBacklogDir);
		if(subDirs.length > 0){
			for (let index in subDirs) {
				let currDir = baseBacklogDir + subDirs[index] + '/';

				if (subDirs[index].match(WORK_SUB_DIR_NAME_REGEX)) {

					let currDirFiles = fs.readdirSync(currDir);
					let isEmptyDir = currDirFiles.length == 0 ? true : false;
					jpegPresent = false;
					if(isEmptyDir == false) {
						jpegPresent = currDirFiles.some(function(element) {
							return element.endsWith('.jpg');
						});
						if(jpegPresent) {
							currDirFiles = currDir + "*.jpg";
							command = `sudo mv ${currDirFiles} ${targetDir}`;
							console.log("Executing command: " + command);
							execSync(command);
						}
					}
				}
				else{
					console.log("Nothing to move from " + currDir);
				}
			}
		}else{
			console.log("No subdirectories present");
		}
	}
}

function removeEmptySubdirectories(baseDir) {
	if(fs.existsSync(baseDir)) {
		let subDirs = getDirectories(baseDir);
		if(subDirs.length > 0) {
			for (let index in subDirs) {
				let currDir = baseDir + subDirs[index] + '/';

				if (subDirs[index].match(WORK_SUB_DIR_NAME_REGEX)) {

					let currDirFiles = fs.readdirSync(currDir);
					let isEmptyDir = currDirFiles.length == 0 ? true : false;
					jpegPresent = false;
					if (isEmptyDir) {
						console.log("Removing Empty directory: " + currDir);
						fs.rmdirSync(currDir);
					}else{
						jpegPresent = currDirFiles.some(function(element){
							return element.endsWith('.jpg');
						});
						if (jpegPresent == false) {
							for (index in currDirFiles){
								currFilePath = currDir + currDirFiles[index];
								console.log("Removing: " + currFilePath);
								fs.unlinkSync(currFilePath);
							}
							fs.rmdirSync(currDir);
						}else{
							console.log("Jpeg files present. Not removing.")
						}
					}
				}
			}
		}else{
			console.log("No Subdirectories present");
		}
	}
}

async function deliverCurrentImageToWebsocket(){
	var image = null;
	if (!cameraConfiguration.mode || cameraConfiguration.mode == "single" || cameraConfiguration.mode == "burst"){
		image = await camera.captureImage();
	}else{
		image = pyImage;
	}
	if (image){
		formatImageForWebsocket(image);
	}
}

function formatImageForWebsocket(image){
	var date = new Date();
	var size = image.size();

	//mark up the image with the crop rectangles
	if(cameraConfiguration.steps) {
		image = singleImageTransforms.markupStepCrops(image, {steps: cameraConfiguration.steps}).images[0].data;
	}

	//don't need huge images for websocket
	var maxHeight = 720
	if(size[0] > maxHeight){
		var aspectRatio = size[1] / size[0];
		var newWidth = aspectRatio * maxHeight;
		image.resize(newWidth, maxHeight);
		size = image.size();
	}

	var websocketMetadata = {
		date: date.getTime(),
		camera: cameraConfiguration.id,
		height: size[0],
		width: size[1],
		type: "frame"
	};

	websocketMetadata.image = image.toBuffer().toString("base64");
	lastImageData = websocketMetadata;

	publishToWebsocketClients(websocketMetadata);
}

async function publishCurrentImage(options){
	var date = new Date();
	var message;
	try {
		var image = await camera.captureSingle();
		message = await imageProcessors.transformImageToMessage(image, options, date.getTime());
	} catch(e){
		console.error(e);
		message = imageProcessors.getErrorMessage(options, date, e.toString());
	}

	mqtt_client.publish(PUBLISH_TOPIC, JSON.stringify(message));
}

async function publishCurrentAEImage(options){

  if (camera.capturing()) {
    return;
  }

	var l_regions = { "_null_region": null };
	if (options.regions!=null) {
		l_regions = options.regions;
	}

	for (var regionName in l_regions) {
		var activeRegion = l_regions[regionName];
		var message;
		try {
			var image = camera.captureAE(options, regionName, activeRegion);

			pyImage = image;

			var activeRegionInfo = null;
			if (activeRegion!=null) {
				activeRegionInfo = {};
				activeRegionInfo[regionName] = activeRegion;
			}
			message = await imageProcessors.transformImageToMessage(image, options, new Date().getTime(), activeRegionInfo);
		} catch(e){
			console.error(e);
			message = imageProcessors.getErrorMessage(options, new Date(), e.toString());
		}

		mqtt_client.publish(PUBLISH_TOPIC, JSON.stringify(message));
	}
}


async function publishPiCameraBaseImage(options){

    if (camera.capturing()) {
      return;
    }

    var message;
    try {
      var image = camera.capturePiCameraBaseImage(options);

	  pyImage = image;

	  message = await imageProcessors.transformImageToMessage(image, options, new Date().getTime());
    } catch(e){
      console.error(e);
      message = imageProcessors.getErrorMessage(options, new Date(), e.toString());
    }

    mqtt_client.publish(PUBLISH_TOPIC, JSON.stringify(message));

}

async function publishSingleAEMotionImage(filePath, options){

	console.log("Publishing one image: " + filePath);
	let fparts = file.split("_");
	let collectionTime = fparts[1];

	let messageStr;

	let date = new Date(collectionTime);
	let image;
	let stats = fs.statSync(filePath);
	let fileSize = stats["size"];
	if(fileSize > 0) {
		try {
			console.log("about to readImage: " + filePath);
			image = cv.readImage(filePath);
			console.log("readImage completed: " + filePath);
			let message = await imageProcessors.transformImageToMessage(image, options, date.getTime());
			console.log("transformImageToMessage completed: "+filePath);
			messageStr = JSON.stringify(message);
			delete message;
			console.log("Message prepared: " + filePath);
		}
		catch(e) {
			console.error(e);
			messageStr = JSON.stringify(imageProcessors.getErrorMessage(options, date, e.toString()));
		}
		finally {
			if (image) {
				image.release();
				delete image;
			}
		}
	}else {
		fs.unlinkSync(filePath);
		console.log("Abnormal File Size: filePath="+filePath+" fileSize="+fileSize);
		return;
	}

	await asyncMqttPublish(mqtt_client, PUBLISH_TOPIC, messageStr);
	await fs.unlink((filePath), (err) => {
		if (err) throw err;
	});
	console.log("Image file was deleted: " + filePath);
}

async function asyncMqttPublish(mqttClient, publishTopic, messageStr) {
	// We are applying Promise pattern before adopting "aysnc-mqtt" package comprehensively
	// please see:
	// https://github.com/mqttjs/async-mqtt/blob/master/index.js

	function makeCallback (resolve, reject) {
		return function (err, data) {
		  if (err)
			reject(err);
		  else resolve(data);
		};
	}

	return new Promise(function(resolve, reject) {
		mqttClient.publish(publishTopic, messageStr, makeCallback(resolve, reject));
	});
}

async function publishSingleAEMotionImageFromList(targetDir, files, fileIndex, options) {
	file = files[fileIndex];
	if (file.startsWith('img') && file.endsWith(".jpg")) {
		filePath = targetDir + file;
		await publishSingleAEMotionImage(filePath, options);
	}
	if (fileIndex<files.length-1) {
		// Delaying the publishing of next image for 100 milliseconds.
		setTimeout( ()=> {
				publishSingleAEMotionImageFromList(targetDir, files, fileIndex+1, options);
			},
			100);
		console.log("heapUsd="+(process.memoryUsage().heapUsed / 1024 / 1024));
	}
}

async function publishAEMotionImages(options) {

	if(fs.existsSync(MOTION_FILES_DIR)) {
		var files = fs.readdirSync(MOTION_FILES_DIR);
		if (files.length > 0){
			targetDir = MOTION_FILE_BACKLOG_ROOT_DIR + WORK_SUB_DIR_NAME_PREFIX + (new Date().getTime()) + '/';
			console.log("Creating Target Dir: " + targetDir);
			fs.mkdirSync(targetDir);
			console.log("Moving files to: " + targetDir);
			currDirFiles = MOTION_FILES_DIR + '*.jpg';
			// Moving all the files to targetDir
			command = `sudo mv ${currDirFiles} ${targetDir}`;
			console.log("Executing: " + command);
			execSync(command);

			//Publishing Images one at a time using deferred recursion
			await publishSingleAEMotionImageFromList(targetDir, files, 0, options);
		}
	}
}

async function publishCurrentImageBurst(options){
	var date = new Date();
	var message;
	try {
		var images = await camera.captureBurst(options.duration || DEFAULT_BURST_DURATION, options.fps || DEFAULT_BURST_FPS);
		message = await imageProcessors.transformImageListToMessage(images, options, date.getTime());
	}catch(e){
		console.error(e);
		message = imageProcessors.getErrorMessage(options, date, e.toString());
	}

	mqtt_client.publish(PUBLISH_TOPIC, JSON.stringify(message));
}

function getCerts(){
	return new Promise(function(resolve, reject){
		if(fs.existsSync("ssl/keys.pkcs12")){
			pem.readPkcs12("ssl/keys.pkcs12", {p12Password:""}, function(err, result){
				resolve({
					serviceKey: result.key,
					certificate: result.cert
				});
			})
		} else {
			pem.createCertificate({days:9999, selfSigned:true, commonName: os.hostname()}, function(err, keys){
				console.log("generating keys");
				if (!fs.existsSync("ssl")){
					fs.mkdirSync("ssl");
				}
				pem.createPkcs12(keys.serviceKey, keys.certificate, "at0ll0gy", function(err, result){
					fs.writeFileSync("ssl/keys.pkcs12", result.pkcs12);
					resolve(keys);
				});
			});
		}
	});
}

// Set up the standard publishing interval

deliverCurrentImageToWebsocket();

if (cameraConfiguration.mode == "ae_motion_binding"){

	// Moving any residual files in any subdirectories to MOTION_FILES_DIR
	moveMotionFilesToTargetDir(MOTION_FILE_BACKLOG_ROOT_DIR, MOTION_FILES_DIR)

	// First time starting the AEMotion instantly
	setTimeout(function(){
		var l_regions = { "_null_region": null };
		if (cameraConfiguration.regions!=null) {
			l_regions = cameraConfiguration.regions;
		}

		for (var regionName in l_regions) {
			var activeRegion = l_regions[regionName];
		}

		camera.startAndMonitorAEMotionProcess(cameraConfiguration.captureCount || false,
						      cameraConfiguration.captureGap || false, cameraConfiguration.threshold || false,
								  cameraConfiguration.optimalBrightness || false, regionName, activeRegion, cameraConfiguration.orientation,
								  cameraConfiguration.heartbeatImages)}, 5);

	//Monitor - Periodically checking that 'motion_detection' python process state (running/terminated)
	setInterval(function(){
		var l_regions = { "_null_region": null };
		if (cameraConfiguration.regions!=null) {
			l_regions = cameraConfiguration.regions;
		}

		for (var regionName in l_regions) {
			var activeRegion = l_regions[regionName];
		}

		camera.startAndMonitorAEMotionProcess(cameraConfiguration.captureCount || false,
						      cameraConfiguration.captureGap || false, cameraConfiguration.threshold || false,
							  cameraConfiguration.optimalBrightness || false, regionName, activeRegion, cameraConfiguration.orientation,
							  cameraConfiguration.heartbeatImages)}, 120000); // Every 2 minutes

	//Publisher - Periodically checking the directory for new images to publish.
	setInterval(function(){
		removeEmptySubdirectories(MOTION_FILE_BACKLOG_ROOT_DIR);
		publishAEMotionImages(cameraConfiguration);
	}, 60000); // Every 1 minute

} else if(cameraConfiguration.interval){
	setTimeout(publishImageBasedOnMode, cameraConfiguration.interval*1000, cameraConfiguration.interval);
} else {
	/* no statement */
	/* as no allowed mode/interval specified. */
	console.log("No allowed 'mode' or 'interval' specified");
}

function publishImageBasedOnMode(timeoutInterval) {
	// Using recursive setTimeout based on cameraConfiguration.interval

	/*
	* Caution: The behaviour when the operation takes more than the interval time
	* needs to be fully tested.
	* Because we switched to using setTimeout from setInterval.
	* -> setInterval waits for operation to get finished and
	* takes operation time in the calculation of next interval.
	* -> While setTimeout executes the operation at a fix time interval.
	*/

    setTimeout(publishImageBasedOnMode, timeoutInterval * 1000, cameraConfiguration.interval);
    if (cameraConfiguration.mode == "burst") {
        publishCurrentImageBurst(cameraConfiguration);
    } else if (cameraConfiguration.mode == 'ae') {
        publishCurrentAEImage(cameraConfiguration);
    } else if (cameraConfiguration.mode == "picamera_base") {
        publishPiCameraBaseImage(cameraConfiguration);
    }
    else {
        publishCurrentImage(cameraConfiguration);
    }
}

// Websockets require faster image cycle

var fastImageInterval;

function startFastImageCollection(){
	if (!fastImageInterval) {
		console.log("websocket client connected, starting fast image polling");
		fastImageInterval = setInterval(function(){
			deliverCurrentImageToWebsocket();
		}, 500);
	}
}

function stopFastImageCollection(){
	if (fastImageInterval) {
		clearInterval(fastImageInterval);
		fastImageInterval = null;
	}
}

var wss;
function publishToWebsocketClients(imageData){
	if (!wss) return;
	var message = JSON.stringify(imageData);
	wss.clients.forEach(function(client){
		if (client.readyState === WebSocket.OPEN && client.authorized) {
	      client.send(message);
	    }
	});
}

//MQTT setup

// MQTT connect callback to subscribe to beacon feeds
mqtt_client.once('connect', function () {
    console.log("Image Collector Started, subscribing to image triggers.");
    mqtt_client.subscribe(TRIGGER_TOPIC);
});

// MQTT message handler
mqtt_client.on('message', function (topic, message) {
    console.log("Image trigger received, collecting image");
    publishCurrentImage({steps:[{name: "trigger", imageTransform:"baseMetadata"}]});
});

// Create the server components for administration

getCerts().then(function(keys){
	//Create HTTP Server
    var server = https.createServer({key: keys.serviceKey, cert: keys.certificate}, function(req, res){
    	var credentials = auth(req);
    	if (!credentials || credentials.name != "alignment" || credentials.pass != "xrayspecs"){
    		res.writeHead(401, {
    			'WWW-Authenticate': 'Basic realm="Atollogy Gateway"'
    		});
    		res.end();
    	} else {
    		res.writeHead(200, {
	        	"Content-Type": "text/html"
		    });
		    res.end(indexPage);
    	}
    }).listen(LISTEN_PORT, function(){
    	console.log("listening on " + server.address().port);
    });

    //Create WebSocket Server (for serving image data)

	wss = new WebSocket.Server({
	    server: server,
	    path: "/imageStream"
	});

	wss.on("connection", function(ws){
		ws.isAlive = true;
		ws.on("message", function(message){
			if(!ws.authorized && message == passwordToken){
				ws.authorized = true;
				startFastImageCollection();
				if(lastImageData){
					ws.send(JSON.stringify(lastImageData));
				}
			}
		});

		ws.on("close", function(){
			if(Array.from(wss.clients).filter(function(client){ return client.authorized}).length == 0){
				console.log("no websocket clients connected, stopping fast image polling");
				stopFastImageCollection();
			}
		});

		ws.on("pong", function(){
			ws.isAlive = true;
		});
	});

	//clean up any dead websocket connections
	setInterval(function(){
		wss.clients.forEach(function(client){
			if (!client.isAlive){
				console.log("terminating unresponsive websocket client");
				return client.terminate();
			}
			client.isAlive = false;
			client.ping("",false);
		});
	}, 30*1000);

});

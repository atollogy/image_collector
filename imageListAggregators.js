const fs = require("fs-extra");
const cv = require("opencv");
const moment = require("moment");
const ffmpeg = require('fluent-ffmpeg');
const stream = require('stream');
const {pctOrValue, rgbToHsl, rgbToHsv, getWhite, lightState, hueToColorName, imageSubDimensionsWithRadius, annotateAndonImage} = require("./imageUtils.js");

//return the metadata for the first image
module.exports.first = function(imageList, parameters){
	return {
		images: [{
			name: "first.jpg",
			data: imageList[0]
		}]
	}
}

//return metadata for all the images
module.exports.all = function(imageList, parameters){
	return {
		images: imageList.map((image, index) => {
			return {
				name: (index+1) + ".jpg",
				data: image
			};
		})
	}
}

//return the metadata for the average of all the images
module.exports.average = function(imageList, parameters){
	var width = imageList[0].width();
	var height = imageList[0].height();
	var channels = imageList[0].channels();

	//if there is an alpha channel, remove it
	imageList = (channels != 4)? imageList : imageList.map((image) => {
		var channelArr = image.split().filter( (elem, index) => index < 3);
		var dest = new cv.Matrix(height, width, cv.Constants.CV_8UC3);
		dest.merge(channelArr);
		return dest;
	});

	var floatType = cv.Constants.CV_32FC3;
	var intType = cv.Constants.CV_8UC3;
	
	var imageF = new cv.Matrix(height, width, floatType);
	var result = new cv.Matrix(height, width, intType);
	var accum = new cv.Matrix(height, width, floatType, [0,0,0]);

	for (var idx in imageList){
		imageList[idx].convertTo(imageF, floatType);
		accum.addWeighted(accum, 1.0, imageF, 1.0 / imageList.length);
	}

	accum.convertTo(result, intType);

	return {
		images: [{
			name: "average.jpg",
			data: result
		}]
	}
}

// determine the state and color of a flashing light at a particular location
// parameters:
//   location: {x,y}
//   radius: Radius of the detection area. Defaults to 10
//   vThreshold: Value threshold for on vs off. Defaults to 0.73
//   onOffRatio: ratio of off to on frames. Defaults to 0.9
//   minFlashingTransitionCount: Minimum number of state changes to be considered flashing. Defaults to 4
module.exports.andonState = async function(imageList, parameters, duration){
	//if there's no location, find the location from the average of the cropped images

	var vThreshold = parameters.vThreshold || 0.73;
	var onOffRatio = parameters.onOffRatio || 0.9;
	var minFlashingTransitionCount = parameters.minFlashingTransitionCount || 4;

	var onOffValues = imageList.map(image =>{
		var dimensions = imageSubDimensionsWithRadius(image, parameters);
		var cropped = image.crop(dimensions.x, dimensions.y, dimensions.width, dimensions.height);

		var channelMeans = cropped.mean(); 
		var [blue, green, red] = channelMeans;

		//find the hue and brightness
		var hsvColor = rgbToHsv(red, green, blue);
		return {h:hsvColor.h, s:hsvColor.s, v:hsvColor.v, r: red, g:green, b:blue, on: hsvColor.v > vThreshold};
	});

	//if the light is on or flashing, get the color for the on frame average
	var toSum = onOffValues.filter(value => value.on);
	if(toSum.length == 0){
		//if the light is off, get the average color for all the frames
		toSum = onOffValues;
	}

	var sums = toSum.reduce((accum, value) => {
		accum.r += value.r;
		accum.g += value.g;
		accum.b += value.b;
		return accum;
	}, {r:0, g:0, b:0});

	var meanHsv = rgbToHsv(sums.r/toSum.length, sums.g/toSum.length, sums.b/toSum.length);
	var whiteTotal = toSum.reduce((tot, value) => {
		tot + getWhite(value.r, value.g, value.b) ? 1 : 0;
		return tot;
	});
	var colorName = hueToColorName(meanHsv.h, whiteTotal > 0);

	//determine flashing, solid, or off
	var counts = {on:0, off:0, transitions:0};
	onOffValues.reduce((accumulator, value) => {
		var newState = value.on ? "on" : "off";

		counts[newState] += 1

		if (newState !== accumulator.last){
			accumulator.transitions += 1;
		}
		accumulator.last = newState;
		return accumulator;
	}, counts);

	var result = {
		counts:counts,
		meanHsv: meanHsv
	};

	// calculate the result
	if (counts.transitions > minFlashingTransitionCount){
		result.state = lightState(true, true); // flashing
	} else if ((counts.on/onOffValues.length) > onOffRatio){
		result.state = lightState(true);
	} else if ((counts.off/onOffValues.length) > onOffRatio) {
		result.state = lightState(false);
	} else {
		result.state = lightState(counts.first == "on");
	}
	result.color = (result.state == lightState(false)) ? undefined : colorName;

	//calculate the annotated image
	var annotatedInputStream = new stream.Readable();
	imageList.forEach(image => {
		annotatedInputStream.push(annotateAndonImage(image, parameters, result).toBuffer());
	});
	annotatedInputStream.push(null);

	var fpsSpeedup = 3;

	var movieBuffer = await new Promise((resolve, reject) => {
		var bufs = [];

		var outputStream = ffmpeg(annotatedInputStream)
			.inputFormat('image2pipe')
			.inputFps(fpsSpeedup * imageList.length / duration)
			.videoCodec('libx264')
			.format('mp4')
			.addOptions([
		      "-movflags frag_keyframe+faststart"
		    ])
			.on('error', (e) => {
				reject(e);
			})
			.pipe()
			.on('data', d => bufs.push(d))
			.on('end', () => {
				resolve(Buffer.concat(bufs));
			});
	});

	return {
		metadata:result,
		images:[{
			name: "annotated.mp4",
			data: movieBuffer
		}]
	};
}

module.exports.log = function(imageList, parameters, duration){
	var now = moment();
	fs.ensureDirSync("./log");
	imageList.forEach( (image, index) => {
		image.save("./log/" + now.format("YYYY-MM-DD-HH-mm-ss")+"-"+index + "-log.jpg");
	});
}

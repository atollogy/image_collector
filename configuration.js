var fs = require("fs-extra");
const { execSync } = require('child_process');

/* Camera Configuration Examples
Single Image Mode:
{
	id: 0,
	orientation: 0,
	interval: 60, // seconds for automatic capture, automatic capture disabled if null
	mode: "single",
	steps: [{name:"default", imageTransform:"baseMetadata", parameters: {}}]
}

Burst Mode:
{
	id: 0,
	orientation: 0,
	interval: 15, // seconds for automatic capture, automatic capture disabled if null
	mode: "burst",
	duration: 3,
	fps: 10,
	steps: [{name:"default", imageAggregate:"first", imageTransform:"baseMetadata", parameters: {crop: {x:"25%", y:"25%", w:"50%",h:"50%"}}}]
}
*/

module.exports.codeVersion = execSync("git rev-parse --short HEAD").toString().trim();

try {
	var bcmLocation = ["bcm.json", "/etc/kvmanager/bcm.json"].find( path => fs.existsSync(path));

	if(bcmLocation){
		var bcm = fs.readJsonSync(bcmLocation);

		module.exports.mqttServer = bcm.svcs[bcm.scode].mqtt_server;
	    module.exports.mqttPort = bcm.svcs[bcm.scode].mqtt_port;
	    module.exports.gatewayId = bcm.gateway_id && /^[0-9A-Fa-f]{12}$/.test(bcm.gateway_id) ? bcm.gateway_id.toLowerCase() : undefined;

	    module.exports.cameraConfiguration = bcm.svcs[bcm.scode].sensorCfg.cameraConfiguration[0];
	    module.exports.configVersion = bcm.svcs[bcm.scode].sensorCfg.version;

	} else {
		console.log("No BCM file configured, running in default test mode.");

		module.exports.cameraConfiguration = {
			id: 0,
			orientation: 0,
			resolution: 1080,
			interval: 60, // seconds for automatic capture, automatic capture disabled if null
			mode: "single",
			steps: [{name:"default", imageTransform:"baseMetadata", parameters: {}}]
		};
		module.exports.configVersion = "development";
	}
} catch(e){
	console.error(e);
}

if (!module.exports.cameraConfiguration){
	console.error("No camera configuration supplied!");
	cameraConfiguration = {
		id:0,
		mode:"single",
		steps:[]
	};
}
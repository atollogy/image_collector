#Pull base image
FROM atldevops/atl_base_image:latest
MAINTAINER atldevops <devops@atollogy.com>
# Install avahi lib and softwares
RUN apt-get -y update
RUN apt-get install -m -y libavahi-client-dev libavahi-core-dev libavahi-compat-libdnssd-dev libopencv-dev 
RUN mkdir -p /opt/image_collector
COPY * /opt/image_collector
RUN rm /opt/image_collector/Dockerfile
WORKDIR /opt/image_collector/
RUN npm install --production
CMD service dbus start && service avahi-daemon start && /usr/bin/node imageCollector.js

var cv = require("opencv");
var fs = require("fs");
var ps = require("ps-node");

const { execSync } = require('child_process');
const { exec } = require('child_process');

// ae_motion_pid.json file stores the process_id of the running python process.
var MOTION_PROCESS_ID_FILE_PATH = "/var/lib/atollogy/ae_camera/ae_motion_pid.json";

//Entry point to the python motion_detection.py
var MOTION_PROCESS_FILE_PATH = "/opt/ae_camera/motion_detection_entry.py";

const SUPPORTED_RESOLUTIONS = {
	//4:3 video resolutions
	240: {w:320, h:240},
	480: {w:640, h:480},      //full sensor (binned)
	720: {w:960, h:720},      //full sensor
	768: {w:1024, h:768},     //full sensor
	972: {w:1296, h:972},     //full sensor (binned)
	1080: {w:1440, h:1080},   //full sensor
	1944: {w:2592, h:1944},   //full sensor native resolution  for pi camera v1
	2464: {w:3280, h:2464},   //full sensor native resolution  for pi camera v2
	//16x9 video resolutions (cropped)
	730: {w:1296, h:730},     //cropped 16x9
	"720p": {w:1280, h:720},  //cropped 16x9
	"1080p": {w:1920, h:1080}, //cropped 16x9

	//tall/reversed versions
	//3:4 video resolution
	320: {w:240, h:320},
	640: {w:480, h:640},      //full sensor (binned)
	960: {w:720, h:960},      //full sensor
	1024: {w:768, h:1024},     //full sensor
	1296: {w:972, h:1296},     //full sensor (binned)
	1440: {w:1080, h:1440},   //full sensor
	2592: {w:1944, h:2592},   //full sensor native resolution  for pi camera v1
	3280: {w:2464, h:3280},   //full sensor native resolution  for pi camera v2
	//9x16 video resolutions (cropped)
	1296: {w:730, h:1296},     //cropped 16x9
	"1280p": {w:720, h:1280},  //cropped 16x9
	"1920p": {w:1080, h:1920}, //cropped 16x9
};

const SUPPORTED_CAMERA_PROPERTIES = [
	"auto_exposure",
	"exposure_time_absolute",
	"white_balance_auto_preset",
	"iso_sensitivity_auto",
	"iso_sensitivity",
	"scene_mode",
	"red_balance",
	"blue_balance",

	// Technet USB Camera drivers
	"exposure_auto",
	"exposure_absolute",
	"white_balance_temperature_auto",
	"white_balance_temperature"
];

function spinUpAEMotionProcess(captureCount = false, captureGap = false, threshold = false,
				   optimalBrightness = false, regionName = null, activeRegion = null, rotation = null,
				   heartbeatImages) {
	args = "";
	if(captureCount) {
		args += ` --captureCount ${captureCount}`;
	}
	if(captureGap) {
		args += ` --captureGap ${captureGap}`;
	}
	if(threshold) {
		args += ` --threshold ${threshold}`;
	}
	if(optimalBrightness) {
		args += ` --optimalBrightness ${optimalBrightness}`;
	}
	if(rotation) {
		args += ` --rotation ${rotation}`;
	}
	if (regionName) {
		if (activeRegion) {
			// copied from ae_camera.py:
			// ...
			// parser.add_argument('--region', nargs=4, type=int, dest='region')
			// ...
			// '''region = (xmin, xmax, ymin, ymax)
			//     region will be centered on the image if it is not set manually'''
			args += ` --regionname ${regionName} `;

			ae_region_list = [ activeRegion.min_x, activeRegion.min_x + activeRegion.width,
							   activeRegion.min_y, activeRegion.min_y + activeRegion.height ];
			// converting a JSON object to a Number List expected by "ae_camera.py"
			args += ` --region ${ae_region_list[0]} ${ae_region_list[1]} ${ae_region_list[2]} ${ae_region_list[3]} `;
		} else {
			console.log("Region coordinates must be passed");
		}
	} else {
		if(activeRegion) {
			console.log("Region name must be passed.");
		}
	}
	if(heartbeatImages) {
		args += ` --heartbeat`;
	}
	command = MOTION_PROCESS_FILE_PATH + args;

	console.log("Executing command : " + command);

	// The process running in exec() might stay alive for
	// 1-2 minutes even after the parent process exits (e.g. kill -9 ,
	// systemctl stop image_collector or systemctl restart image_collector)
	// The behavior needs to be tested(verified) further
	// and possible alternative should be searched if behavior persists

	var process = exec(command);
	processId = process.pid;
	return processId;
}

function motionFileCheck(){
	return fs.existsSync(MOTION_PROCESS_ID_FILE_PATH);
}

function isV4l2Installed(){
	try {
		execSync("command -v v4l2-ctl");
		return true;
	} catch(e){
		return false;
	}
}

function isInt(value) {
  return !isNaN(value) &&
  	parseInt(Number(value)) == value &&
    !isNaN(parseInt(value, 10));
}

function setCameraProperty(id, name, value){
	//sanitize the inputs because we are going to execute a shell command
	//id must be an integer
	//name must be an allowed string
	//value must be an integer
	if( isInt(id) && SUPPORTED_CAMERA_PROPERTIES.some(x => x == name) && isInt(value)){
		try {
			execSync(`v4l2-ctl -d /dev/video${id} -c ${name}=${value}`);
		} catch (e){
			console.error("an error occurred setting property " + name + " to " + value + " for camera " + id);
		}
	} else {
		console.error("invalid parameters for camera property setter: " + id + ": " + name + "=" + value);
	}
}

async function captureImage(){
	return new Promise((resolve, reject) => {
		this.cvCamera.read((err, im)=>{
			if (err){
				reject(err);
			} else {
				if(this.orientation){
					im.rotate(this.orientation);
				}
				resolve(im);
			}
		});
	});
}

function startAndMonitorAEMotionProcess(captureCount = false, captureGap = false, threshold = false,
					optimalBrightness = false, regionName = null, activeRegion = null, rotation = null,
					heartbeatImages){
	var processId = undefined;
	if (motionFileCheck()){
		fs.readFile(MOTION_PROCESS_ID_FILE_PATH, 'utf8', function(err, contents) {
			if(err){
				console.log('Error while reading a file : ' + err);
			}else{
				try{
					json_obj = JSON.parse(contents);
					try{
						if ("ae_motion_pid" in json_obj){
							processId = json_obj["ae_motion_pid"];
							console.log("Process ID present : " + json_obj["ae_motion_pid"]);
							ps.lookup({ pid: processId }, function(err, resultList ) {
								if (err) {
									console.log("Look up error" + err);
								}
								var process = resultList[0];
								if(process){
									console.log(JSON.stringify(process));
									console.log( 'Process present with PID: %s', process.pid);
								}
								else {
									console.log("Process not running.");
									processId = spinUpAEMotionProcess(captureCount, captureGap, threshold,
													  optimalBrightness, regionName, activeRegion, rotation,
													  heartbeatImages);
									console.log("New process spun up with processId : " + processId);
								}
							});
						}
						else{
							console.log("Process_id not present. Spining up new process");
							processId = spinUpAEMotionProcess(captureCount, captureGap, threshold,
											  optimalBrightness, regionName, activeRegion, rotation,
											  heartbeatImages);
							console.log("New process spun up with processId : " + processId);
						}
					}
					catch(e){
						// other types of error, due to ps logic or other logic
						// Not spin up a new process in this case
						console.log("Process Managment error: "+e);
					}
				}catch (e){
					// JSON parsing error; most likely the Python process died when writing the JSON file
					console.log("JSON Parsing Error. Spining up new process");
					processId = spinUpAEMotionProcess(captureCount, captureGap, threshold,
									  optimalBrightness, regionName, activeRegion, rotation,
									  heartbeatImages);
					console.log("New process spun up with processId : " + processId);
				}
			}
		});
	}else{
		console.log("File not present. Spining up new process");
		processId = spinUpAEMotionProcess(captureCount, captureGap, threshold,
						  optimalBrightness, regionName, activeRegion, rotation,
						  heartbeatImages);
		console.log("New process spun up with processId : " + processId);
	}
}

var capturingPicameraImage = false;

function capturing() {
  return capturingPicameraImage;
}

function captureAE(options, regionName=null, activeRegion=null){
	hdr = options.hdr || false;
	rotation = options.orientation;
	optimalBrightness = options.optimalBrightness || false;
	[width, height] = ImageResolution(options);

	command = `/opt/ae_camera/ae_camera.py --resolution ${width} ${height}`;

	if (rotation != null) {
		command += ` --rotation ${rotation}`;
	}
	if (hdr) {
		command += ` --hdr`;
	}
	if (optimalBrightness) {
		command += ` --optbrightness ${optimalBrightness}`;
	}
	if (regionName) {
		command += ` --regionname ${regionName} `;
	}
	if (activeRegion) {
		// copied from ae_camera.py:
		// ...
		// parser.add_argument('--region', nargs=4, type=int, dest='region')
		// ...
		// '''region = (xmin, xmax, ymin, ymax)
		//     region will be centered on the image if it is not set manually'''
		//

		ae_region_list = [ activeRegion.min_x, activeRegion.min_x + activeRegion.width,
						   activeRegion.min_y, activeRegion.min_y + activeRegion.height ]
		// converting a JSON object to a Number List expected by "ae_camera.py"
		command += ` --region ${ae_region_list[0]} ${ae_region_list[1]} ${ae_region_list[2]} ${ae_region_list[3]} `;
	}
	console.log("executing: " + command);

	try {
    capturingPicameraImage = true;
		var img =  cv.readImage(execSync(command));
    capturingPicameraImage = false;
		return img;
	} catch (e) {
		console.error(`an error occured while calling ${command}`);
		console.error(e);
    capturingPicameraImage = false;
	}
}

function capturePiCameraBaseImage(options){

  [width, height] = ImageResolution(options);
  var command = `/opt/ae_camera/picamera_base.py --resolution ${width} ${height}`;

  if (options.brightness) {
  	command += ` --brightness ${options.brightness}`;
  }

  if (options.framerate) {
    command += ` --framerate ${options.framerate}`;
  }

  if (options.orientation) {
    command += ` --rotation ${options.orientation}`;
  }

  if (options.exposureMode) {
    command += ` --exposure_mode ${options.exposureMode}`;
  }

  if (options.exposureTime) {
    command += ` --shutter_speed ${options.exposureTime}`;
  }

  if (options.iso) {
    command += ` --iso ${options.iso}`;
  }

  console.log("executing: " + command);

  try {
    capturingPicameraImage = true;
    var img =  cv.readImage(execSync(command));
    capturingPicameraImage = false;
    return img;
  } catch (e) {
    console.error(`an error occurred while calling ${command}`);
    console.error(e);
    capturingPicameraImage = false;
  }
}

async function sleep(delay){
	return new Promise((resolve, reject) => {
		setTimeout(()=>{
			resolve();
		}, delay);
	});
}

async function captureSequence(imageCount, fps, discard){
	var imagesCollected = [];
	var interval = 1000 / fps;

	while (imagesCollected.length < imageCount){
		var start = (new Date()).getTime();
		var nextImage = await this.captureImage();
		imagesCollected.push(nextImage);

		if(discard){
			nextImage.release();
		}

		if (imagesCollected.length < imageCount || discard){
			//sleep until next collection time
			var now = (new Date()).getTime();
			var diff = now - start;
			var remaining = Math.max(interval - diff, 0);
			if (remaining == 0 && !discard){
				console.log("missed frame rate request: " + diff +"ms to retrieve last frame");
			}
			await sleep(remaining);
		}
	}

	return discard ? null : imagesCollected;
}

async function clearBuffer(frameCount, fps){
	return await this.captureSequence(frameCount, fps || 100, true);
}

async function captureSingle(){
	await this.clearBuffer(5);
	return await this.captureImage();
}

async function captureBurst(duration, fps){
	//clear the buffer first with a quick burst of 5
	await this.clearBuffer(5, fps);
	//now collect the burst
	var imageCount = (duration * fps) || 1;
	return await this.captureSequence(imageCount, fps);
}

function configurePicam(cameraSource, params){
	params = params || {};
	id = cameraSource.id;
	if (params.iso){
		var isoSetting = [0,100,200,400,800].indexOf(params.iso);
		if (isoSetting){
			//hack to get the setting to take effect: first turn on auto exposure
			setCameraProperty(id, "auto_exposure", 0);

			console.log("setting ISO to " + params.iso);
			setCameraProperty(id, "iso_sensitivity_auto", 0); //turn off auto ISO
			setCameraProperty(id, "iso_sensitivity", isoSetting);

			//hack part 2: wait briefly for the setting to have an effect by capturing some frames
			for (var i=0; i<15; i++){
				cameraSource.cvCamera.readSync();
				execSync('sleep .1');
				// We are not doing async / await sleep here ...
			}

			//hack part 3: if manual exposure is set, auto exposure will be turned off below
		} else {
			console.log("invalid ISO setting: " + params.iso);
		}
	}

	if (params.exposureTime){
		var exposureTime = params.exposureTime;
		if(params.fps){
			//fps implies an upper bound on the exposure time
			//exposure time is measured in 100μs units
			var exposureUpperBound = (1.0 / params.fps) * 10000;
			exposureTime = Math.min(exposureTime, exposureUpperBound);
			if(params.exposureTime > exposureTime){
				console.log("Configured exposure time is too long for the configured fps setting. Using the max value for the fps setting.");
			}
		}
		if (exposureTime <= 0 || exposureTime > 10000){
			console.log("Configured exposure time of " + exposureTime + " is not valid.");
			exposureTime = Math.max(1, Math.min(exposureTime, 10000));
		}
		console.log("Setting exposure time for camera " + id + " to " + exposureTime + " 100μs units");
		setCameraProperty(id, "auto_exposure", 1); //turn off auto exposure
		setCameraProperty(id, "exposure_time_absolute", params.exposureTime);
	}

	if (params.whiteBalance && typeof params.whiteBalance === 'object' && params.whiteBalance.red !== undefined && params.whiteBalance.blue !== undefined){
		console.log("Setting white balance for camera " + id + " to red: " + params.whiteBalance.red + ", blue: " + params.whiteBalance.blue);
		setCameraProperty(id, "white_balance_auto_preset", 0); //turn off auto white balance
		setCameraProperty(id, "red_balance", params.whiteBalance.red);
		setCameraProperty(id, "blue_balance", params.whiteBalance.blue);
	}
}

function configureUSBcam(cameraSource, params){
	params = params || {};
	id = cameraSource.id;

	if (params.exposureTime){
		var exposureTime = params.exposureTime;
		if(params.fps){
			//fps implies an upper bound on the exposure time
			//exposure time is measured in 100μs units
			var exposureUpperBound = (1.0 / params.fps) * 10000;
			exposureTime = Math.min(exposureTime, exposureUpperBound);
			if(params.exposureTime > exposureTime){
				console.log("Configured exposure time is too long for the configured fps setting. Using the max value for the fps setting.");
			}
		}
		if (exposureTime <= 50 || exposureTime > 10000){
			console.log("Configured exposure time of " + exposureTime + " is not valid.");
			exposureTime = Math.max(50, Math.min(exposureTime, 10000));
		}
		console.log("Setting exposure time for camera " + id + " to " + exposureTime + " 100μs units");
		setCameraProperty(id, "exposure_auto", 1); //turn off auto exposure
		setCameraProperty(id, "exposure_absolute", params.exposureTime);
	}

	if (params.whiteBalance){
		var whiteBalance = params.whiteBalance;
		console.log("Setting white balance for camera " + id + " to " + whiteBalance);
		setCameraProperty(id, "white_balance_temperature_auto", 0); //turn off auto white balance
		setCameraProperty(id, "white_balance_temperature", whiteBalance);
	}
}

function getCameraConfigurer(deviceIndex){
	//parse the output of v4l2-ctl --list-devices to get the description associated with our device id
	var device_info = execSync('v4l2-ctl --list-devices').toString()
		.split('\n')
		.map( x => x.trim() )
		.filter( x => x != '' )
		.reduce( (agg, val, index) => {
			if(index % 2 == 0){
				agg.push({desc:val});
			} else {
				agg[agg.length-1].deviceId = val;
			}
			return agg;
		}, [])
		.filter( x => x.deviceId ==`/dev/video${deviceIndex}` )
		.map( x => x.desc)[0];

	if (device_info.toLowerCase().includes('usb')) {
		console.log("Setting USB camera configurations for : " + device_info);
		return configureUSBcam;
	} else if (device_info.toLowerCase().includes('mmal' || 'bcm')){
		console.log("Setting PI camera configurations for : " + device_info);
		return configurePicam;
	} else {
		return function(){
			console.error(`No camera source found / Unknown Camera Source: ${device_info}`);
		};
	}
}

function ImageResolution(params){
	res = params.resolution?params.resolution:0;
	if(SUPPORTED_RESOLUTIONS[res]){
		return [SUPPORTED_RESOLUTIONS[res].w, SUPPORTED_RESOLUTIONS[res].h];
	}else{
		return [res, res];
	}
}

function CameraSource(id, params){
	params = params || {};
	this.id = id;
	this.orientation = params.orientation || 0;

	if (!params.mode || params.mode == "single" || params.mode == "burst"){
		[width, height] = ImageResolution(params);
		this.cvCamera = new cv.VideoCapture(this.id);
		if(width > 0 && height > 0){
			console.log("setting resolution of camera " + id + " to " + params.resolution);
			this.cvCamera.setWidth(width);
			this.cvCamera.setHeight(height);
			console.log("resolution of camera " + id + " set to " + this.cvCamera.getWidth() + "x"+ this.cvCamera.getHeight());
		}else{
			console.error("invalid resolution supplied: " + res);
		}
		if (isV4l2Installed()){
			getCameraConfigurer(id)(this, params);
		}
	}

	this.captureImage = captureImage;
	this.captureAE = captureAE;
	this.capturePiCameraBaseImage = capturePiCameraBaseImage;
	this.startAndMonitorAEMotionProcess = startAndMonitorAEMotionProcess;
	this.captureSingle = captureSingle;
	this.captureBurst = captureBurst;
	this.captureSequence = captureSequence;
	this.clearBuffer = clearBuffer;
  this.capturing = capturing;
}

module.exports = CameraSource;

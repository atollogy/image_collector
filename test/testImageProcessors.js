import test from 'ava'
import imageProcessor from "../imageProcessors.js"
import {readImage, readImageFromBuffer} from "../imageUtils.js"

test("baseMetadata transform", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageToMessage(image, {id:0, mode:"single", interval:60, steps:[{name:"one", imageTransform:"baseMetadata"}]}, now);

	t.is(message.type, "single");
	t.is(message.steps.length, 1);
	t.truthy(message.steps[0].output);
	t.is(message.steps[0].output.height, 720);
	t.is(message.steps[0].output.width, 1280);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 1);
});

test("baseMetadata transform with a crop", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageToMessage(image, {id:0, mode:"single", interval:60, steps:[{name:"one", imageTransform:"baseMetadata", parameters: {crop:{x:10, y:10, h:100, w:100}}}]}, now);

	t.is(message.type, "single");
	t.is(message.steps.length, 1);

	t.is(message.steps[0].output.height, 100);
	t.is(message.steps[0].output.width, 100);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 1);
	t.is(message.images[0].stepName, "one");
	t.is(message.images[0].name, "image.jpg");
});

test("specify crop in percentages", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageToMessage(image, {id:0, mode:"single", interval:60, steps:[{name:"one", imageTransform:"baseMetadata", parameters: {crop:{x:"10%", y:"10%", h:"25%", w:"25%"}}}]}, now);

	t.is(message.type, "single");
	t.is(message.steps.length, 1);

	t.is(message.steps[0].output.height, 180);
	t.is(message.steps[0].output.width, 320);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 1);
	t.is(message.images[0].stepName, "one");
	t.is(message.images[0].name, "image.jpg");
});

test("clip oversized crops", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageToMessage(image, {id:0, mode:"single", interval:60, steps:[{name:"one", imageTransform:"baseMetadata", parameters: {crop:{x:"75%", y:"75%", h:"50%", w:"50%"}}}]}, now);

	t.is(message.type, "single");
	t.is(message.steps.length, 1);

	t.is(message.steps[0].output.height, 180);
	t.is(message.steps[0].output.width, 320);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 1);
	t.is(message.images[0].stepName, "one");
	t.is(message.images[0].name, "image.jpg");
});

test("single image multiple steps", async t =>{
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var config = {id:0, mode:"single", interval:60, steps:[{
		name: "one", 
		imageTransform: "baseMetadata"
	},{
		name: "two",
		imageTransform: "baseMetadata"
	}]};

	var message = await imageProcessor.transformImageToMessage(image, config, now);

	t.is(message.type, "single");
	t.is(message.steps.length, 2);

	t.is(message.steps[0].collectionInterval, 60000);
	t.is(message.steps[1].collectionInterval, 60000);

	t.is(message.images.length, 2);

	t.is(message.images[0].stepName, "one");
	t.is(message.images[0].name, "image.jpg");

	t.is(message.images[1].stepName, "two");
	t.is(message.images[1].name, "image.jpg");
});

test("second step not affected by first step", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var config = {id:0, mode:"single", steps:[{
		name: "one", 
		imageTransform: "thumbnail"
	},{
		name: "two",
		imageTransform: "baseMetadata"
	}]};

	var message = await imageProcessor.transformImageToMessage(image, config, now);

	t.is(message.type, "single");
	t.is(message.steps.length, 2);

	var image0 = await readImageFromBuffer(Buffer.from(message.images[0].data, "base64"));

	t.is(image0.height(), 72);
	t.is(image0.width(), 128);

	t.is(message.steps[1].output.height, 720);
	t.is(message.steps[1].output.width, 1280);
});

test("first transform", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageListToMessage([image], {id:0, mode:"burst", interval:60, steps:[{name:"one", imageAggregate: "first", imageTransform:"baseMetadata"}]}, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 1);
	t.truthy(message.steps[0].output);
	t.is(message.steps[0].output.height, 720);
	t.is(message.steps[0].output.width, 1280);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 1);
});

test("first transform with a crop", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageListToMessage([image], {id:0, mode:"burst", interval:60, steps:[{name:"one", imageAggregate: "first", imageTransform:"baseMetadata", parameters: {crop:{x:10, y:10, h:100, w:100}}}]}, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 1);
	t.truthy(message.steps[0].output);
	t.is(message.steps[0].output.height, 100);
	t.is(message.steps[0].output.width, 100);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 1);
});

test("all transform returns all images", async t => {
	var image = await readImage("./testData/test1.jpg");
	var image2 = await readImage("./testData/test2.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageListToMessage([image, image2], {id:0, mode:"burst", interval:60, steps:[{name:"one", imageAggregate: "all"}]}, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 1);
	//t.truthy(message.steps[0].output);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 2);
});

test("all transform with a crop returns all the images cropped", async t => {
	var image = await readImage("./testData/test1.jpg");
	var image2 = await readImage("./testData/test2.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageListToMessage([image, image2], {id:0, mode:"burst", interval:60, steps:[{name:"one", imageAggregate: "all", parameters: {crop:{x:10, y:10, h:100, w:100}}}]}, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 1);
	//t.truthy(message.steps[0].output);
	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 2);

	var image0 = await readImageFromBuffer(Buffer.from(message.images[0].data, "base64"));
	var image1 = await readImageFromBuffer(Buffer.from(message.images[1].data, "base64"));

	t.deepEqual(image0.size(), [100,100]);
	t.deepEqual(image1.size(), [100,100]);
});

test("all transform with a singleImage transform returns all the images transformed", async t => {
	var image = await readImage("./testData/test1.jpg");
	var image2 = await readImage("./testData/test2.jpg");

	var now = new Date();

	var message = await imageProcessor.transformImageListToMessage([image, image2], {id:0, mode:"burst", interval:60, steps:[{name:"one", imageAggregate: "all", imageTransform:"thumbnail"}]}, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 1);

	t.is(message.steps[0].collectionInterval, 60000);

	t.is(message.images.length, 2);

	t.is(message.images[0].name, "1.jpg");
	t.is(message.images[1].name, "2.jpg");

	var image0 = await readImageFromBuffer(Buffer.from(message.images[0].data, "base64"));
	var image1 = await readImageFromBuffer(Buffer.from(message.images[1].data, "base64"));
	t.deepEqual(image0.size(), [72, 128]);
	t.deepEqual(image1.size(), [72, 128]);
});

test("burst multiple steps", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var config = {
		id:0, 
		mode:"burst", 
		interval:60,
		steps:[{
			name: "one", 
			imageAggregate: "first", 
			imageTransform:"baseMetadata"
		}, {
			name: "two",
			imageAggregate: "first", 
			imageTransform:"baseMetadata"
		}]
	};

	var message = await imageProcessor.transformImageListToMessage([image], config, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 2);
	t.truthy(message.steps[0].output);
	t.is(message.steps[0].output.height, 720);
	t.is(message.steps[0].output.width, 1280);
	t.is(message.steps[0].collectionInterval, 60000);

	t.truthy(message.steps[1].output);
	t.is(message.steps[1].output.height, 720);
	t.is(message.steps[1].output.width, 1280);
	t.is(message.steps[1].collectionInterval, 60000);

	t.is(message.images.length, 2);
	t.is(message.images[0].stepName, "one");
	t.is(message.images[0].name, "first.jpg");

	t.is(message.images[1].stepName, "two");
	t.is(message.images[1].name, "first.jpg");
});

test("burst second step not affected by first step", async t => {
	var image = await readImage("./testData/test1.jpg");

	var now = new Date();

	var config = {
		id:0, 
		mode:"burst",
		steps:[{
			name: "one", 
			imageAggregate: "first", 
			imageTransform:"thumbnail"
		}, {
			name: "two",
			imageAggregate: "first", 
			imageTransform:"baseMetadata"
		}]
	};

	var message = await imageProcessor.transformImageListToMessage([image], config, now);

	t.is(message.type, "burst");
	t.is(message.steps.length, 2);

	var image0 = await readImageFromBuffer(Buffer.from(message.images[0].data, "base64"));

	t.is(image0.height(), 72);
	t.is(image0.width(), 128);

	t.truthy(message.steps[1].output);
	t.is(message.steps[1].output.height, 720);
	t.is(message.steps[1].output.width, 1280);

	t.is(message.images.length, 2);
});

test("construct an error message", async t =>{
	var config = {
		id:0, 
		mode:"burst",
		steps:[{
			name: "one", 
			imageAggregate: "first", 
			imageTransform:"thumbnail"
		}, {
			name: "two",
			imageAggregate: "first", 
			imageTransform:"baseMetadata"
		}]
	};
	var now = new Date();

	var message = await imageProcessor.getErrorMessage(config, now, "an error occurred");

	t.is(message.type, 'burst');
	t.is(message.steps.length, 1);
	t.is(message.steps[0].name, 'error');
	t.is(message.steps[0].output.message, 'an error occurred');
});

test.serial("subInterval configuration only triggers every nth run", async t => {
	var image = await readImage("./testData/test1.jpg");
	var now = new Date();

	var config = {id:0, mode:"single", interval:60, steps:[{
		name: "one", 
		imageTransform: "baseMetadata"
	},{
		name: "two",
		imageTransform: "baseMetadata",
		subInterval: 2
	}]};

	var message = await imageProcessor.transformImageToMessage(image, config, now);
	t.is(message.steps.length, 2);
	t.is(message.steps[0].collectionInterval, 60000);
	t.is(message.steps[1].collectionInterval, 120000);

	message = await imageProcessor.transformImageToMessage(image, config, now);
	t.is(message.steps.length, 1);
	t.is(message.steps[0].collectionInterval, 60000);

	message = await imageProcessor.transformImageToMessage(image, config, now);
	t.is(message.steps.length, 2);
	t.is(message.steps[0].collectionInterval, 60000);
	t.is(message.steps[1].collectionInterval, 120000);

	imageProcessor._resetInternalState();
});

test.serial("subInterval burst step configuration only triggers every nth run", async t => {
	var image = await readImage("./testData/test1.jpg");
	var now = new Date();

	var config = {
		id:0, 
		mode:"burst", 
		interval:60,
		steps:[{
			name: "one", 
			imageAggregate: "first", 
			imageTransform:"baseMetadata"
		}, {
			name: "two",
			imageAggregate: "first", 
			imageTransform:"baseMetadata",
			subInterval: 2
		}]
	};

	var message = await imageProcessor.transformImageListToMessage([image], config, now);
	t.is(message.steps.length, 2);
	t.is(message.steps[0].collectionInterval, 60000);
	t.is(message.steps[1].collectionInterval, 120000);

	message = await imageProcessor.transformImageListToMessage([image], config, now);
	t.is(message.steps.length, 1);
	t.is(message.steps[0].collectionInterval, 60000);

	message = await imageProcessor.transformImageListToMessage([image], config, now);
	t.is(message.steps.length, 2);
	t.is(message.steps[0].collectionInterval, 60000);
	t.is(message.steps[1].collectionInterval, 120000);

	imageProcessor._resetInternalState();
});
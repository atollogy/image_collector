import test from 'ava'
import config from "../configuration.js"

test("configVersion is a 7 char string", async t => {
	t.is(typeof config.codeVersion, "string");
	t.is(config.codeVersion.length, 7);
});

import test from 'ava'
import cv from 'opencv'
import transforms from "../singleImageTransforms.js"
import {readImage} from "../imageUtils.js"

test("baseMetadata", async t => {
	var image = await readImage("./testData/andon/off.jpg");

	var results = transforms.baseMetadata(image);

	t.is(results.metadata.height, 108);
	t.is(results.metadata.width, 51);
	t.is(results.images.length, 1);
});

test("averageRGB", async t =>{
	var image = await readImage("./testData/avgrgb/1.png");

	var results = transforms.averageRGB(image);

	function round(x){
		return Math.round(10*x)/10;
	}

	t.is(results.metadata.r.toFixed(2), "99.61");
	t.is(results.metadata.g.toFixed(2), "99.61");
	t.is(results.metadata.b.toFixed(2), "99.61");
});

test("thumbnail", async t =>{
	var image = await readImage("./testData/andon/off.jpg");

	var results = transforms.thumbnail(image, {});

	var resultImage = results.images[0].data;
	t.is(resultImage.height(), 11);
	t.is(resultImage.width(), 5);
});

const andonConfig = {
	red: {location:{x:27, y:16}, radius:9},
	amber: {location: {x:29, y:35}, radius:9},
	green: {location: {x:30, y:56}, radius:9},
	blue: {location: {x:32, y:76}, radius:9}
};

test("andonState off", async t =>{
	var image = await readImage("./testData/andon/off.jpg");

	var result = transforms.andonState(image, andonConfig.red);

	t.is(result.metadata.state, "off");
});

test("andonState on", async t =>{
	var image = await readImage("./testData/andon/on-red.jpg");

	var result = transforms.andonState(image, andonConfig.red);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "red");
});
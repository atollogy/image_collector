import test from 'ava'
import imageUtils from "../imageUtils.js"

test("pctOrValue returns a value", t => {
	t.is(imageUtils.pctOrValue(5,10), 5);
});

test("pctOrValue returns a value from a string", t => {
	t.is(imageUtils.pctOrValue("5",10), 5);
});

test("pctOrValue returns a value from a percentage", t => {
	t.is(imageUtils.pctOrValue("50%",10), 5);
});

test("pctOrValue returns a value clipped to 0", t => {
	t.is(imageUtils.pctOrValue(-5,10), 0);
	t.is(imageUtils.pctOrValue("-5",10), 0);
	t.is(imageUtils.pctOrValue("-50%",10), 0);
});

test("pctOrValue returns a value clipped to the max", t => {
	t.is(imageUtils.pctOrValue(11,10), 10);
	t.is(imageUtils.pctOrValue("11",10), 10);
	t.is(imageUtils.pctOrValue("150%",10), 10);
});

test("rgbToHsl", t =>{
	var result = imageUtils.rgbToHsl(32,96,35);
	t.is(Math.round(result.h), 123);
	t.is(Math.round(result.s * 100), 50);
	t.is(Math.round(result.l * 100), 25);
});

test("rgbToHsv", t =>{
	var result = imageUtils.rgbToHsv(32,96,35);
	t.is(Math.round(result.h), 123);
	t.is(Math.round(result.s * 100), 67);
	t.is(Math.round(result.v * 100), 38);
});

test("getWhite", t =>{
	t.is(imageUtils.getWhite(192, 192, 192), false);
	t.is(imageUtils.getWhite(192, 193, 193), false);
	t.is(imageUtils.getWhite(193, 193, 193), true);
	t.is(imageUtils.getWhite(194, 194, 194), true);
	t.is(imageUtils.getWhite(195, 193, 193), true);
	t.is(imageUtils.getWhite(196, 193, 193), false);
	t.is(imageUtils.getWhite(200, 197, 197), true);
	t.is(imageUtils.getWhite(201, 197, 197), false);
	t.is(imageUtils.getWhite(205, 201, 201), true);
	t.is(imageUtils.getWhite(206, 201, 201), false);
	t.is(imageUtils.getWhite(255, 241, 241), true);
	t.is(imageUtils.getWhite(256, 241, 241), false); // no bounds checking so this is ok
	t.is(imageUtils.getWhite(255, 255, 255), true);
	t.is(imageUtils.getWhite(0, 0, 0), false);
});

test("lightState", t =>{
	t.is(imageUtils.lightState(false), "off");
	t.is(imageUtils.lightState(true), "solid");
	t.is(imageUtils.lightState(false, false), "off");
	t.is(imageUtils.lightState(true, false), "solid");
	t.is(imageUtils.lightState(true, true), "flashing");
	t.is(imageUtils.lightState(false, true), "flashing");
});

test("hueToColorName", t =>{
	t.is(imageUtils.hueToColorName(-1, true), "white");
	t.is(imageUtils.hueToColorName(0, true), "white");
	t.is(imageUtils.hueToColorName(120, true), "white");
	t.is(imageUtils.hueToColorName(240, true), "white");
	t.is(imageUtils.hueToColorName(360, true), "white");
	t.is(imageUtils.hueToColorName(361, true), "white");
	t.is(imageUtils.hueToColorName(-1, false), "");
	t.is(imageUtils.hueToColorName(0, false), "red");
	t.is(imageUtils.hueToColorName(120, false), "green");
	t.is(imageUtils.hueToColorName(240, false), "blue");
	t.is(imageUtils.hueToColorName(360, false), "red");
	t.is(imageUtils.hueToColorName(361, false), "");

	t.is(imageUtils.hueToColorName(0), "red");
	t.is(imageUtils.hueToColorName(360), "red");

	t.is(imageUtils.hueToColorName(50), "amber");

	t.is(imageUtils.hueToColorName(100), "green");

	t.is(imageUtils.hueToColorName(175), "blue");
	t.is(imageUtils.hueToColorName(200), "blue");

	t.is(imageUtils.hueToColorName(300), "magenta");

	t.is(imageUtils.hueToColorName(-1), "");
	t.is(imageUtils.hueToColorName(361), "");
});

test("imageSubDimensionsWithRadius", async t=>{
	let image = await imageUtils.readImage('./testData/andon2/1.jpg');

	var result = imageUtils.imageSubDimensionsWithRadius(image, {location:{x:'50%', y:'50%'}, radius:10});

	t.is(result.width, 20);
	t.is(result.height, 20);
	t.is(result.x, 70);
	t.is(result.y, 50);
	t.is(result.radius, 10);
});
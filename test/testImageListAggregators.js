import test from 'ava'
import transforms from "../imageListAggregators.js"
import {readImage, show} from "../imageUtils.js"

// ----- Basic Aggregations ---- //

test("first", async t => {
	var one = await readImage("./testData/avgrgb/1.png"); //white in upper left
	var two = await readImage("./testData/avgrgb/5.png"); //all black

	t.is(one.getPixel(2,2)[0], 255);
	t.is(two.getPixel(2,2)[0], 0);

	var result = await transforms.first([one, two], {});

	t.is(result.images[0].name, "first.jpg");

	var [b0,g0,r0] = result.images[0].data.getPixel(2,2);

	t.is(b0, 255);
	t.is(g0, 255);
	t.is(r0, 255);
});

test("average", async t => {
	var one = await readImage("./testData/avgrgb/1.png"); //white in upper left
	var two = await readImage("./testData/avgrgb/2.png"); //white in upper right
	var three = await readImage("./testData/avgrgb/3.png"); //white in lower left
	var four = await readImage("./testData/avgrgb/4.png"); //white in lower right
	var five = await readImage("./testData/avgrgb/5.png"); //all black

	t.is(one.getPixel(2,2)[0], 255);
	t.is(five.getPixel(2,2)[0], 0);

	var result = await transforms.average([one, two, three, four, five], {});

	//await show(result.images[0].data);

	t.is(result.images[0].name, "average.jpg");

	var [bCenter, gCenter, rCenter] = result.images[0].data.getPixel(8,8);

	var [bTopLeft, gTopLeft, rTopLeft] = result.images[0].data.getPixel(3,3);

	var [bTopCenter, gTopCenter, rTopCenter] = result.images[0].data.getPixel(3,8);

	t.is(bCenter, 204);
	t.is(gCenter, 204);
	t.is(rCenter, 204);

	t.is(bTopLeft,51);
	t.is(gTopLeft,51);
	t.is(rTopLeft,51);

	t.is(bTopCenter,102);
	t.is(gTopCenter,102);
	t.is(rTopCenter,102);
});

test("all", async t => {
	var one = await readImage("./testData/avgrgb/1.png"); //white in upper left
	var two = await readImage("./testData/avgrgb/5.png"); //all black

	var result = await transforms.all([one, two], {});

	t.is(result.images.length, 2);

	t.is(result.images[0].name, "1.jpg");
	t.is(result.images[1].name, "2.jpg");

	var [b0,g0,r0] = result.images[0].data.getPixel(2,2);

	t.is(b0, 255);
	t.is(g0, 255);
	t.is(r0, 255);

	var [b1,g1,r1] = result.images[1].data.getPixel(2,2);

	t.is(b1, 0);
	t.is(g1, 0);
	t.is(r1, 0);

});

// ----- Andon Light State Detection ---- //

const andonConfig = {
	red: {location:{x:27, y:16}, radius:9},
	amber: {location: {x:29, y:35}, radius:9},
	green: {location: {x:30, y:56}, radius:9},
	blue: {location: {x:32, y:76}, radius:9}
};

test("andon off", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/off.jpg");
		frames.push(frame);
	}

	var resultRed = await transforms.andonState(frames, andonConfig.red);
	var resultAmber = await transforms.andonState(frames, andonConfig.amber);
	var resultGreen = await transforms.andonState(frames, andonConfig.green);
	var resultBlue = await transforms.andonState(frames, andonConfig.blue);

	t.is(resultRed.metadata.state, "off");
	t.is(resultAmber.metadata.state, "off");
	t.is(resultGreen.metadata.state, "off");
	t.is(resultBlue.metadata.state, "off");
});


test("andon on red", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/on-red.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.red);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "red");
});

test("andon on amber", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/on-amber.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.amber);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "amber");
});

test("andon on green", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/on-green.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.green);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "green");
});

test("andon on blue", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/on-blue.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.blue);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "blue");
});

test("andon flashing red", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/flashing-red-" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.red);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "red");
});

test("andon flashing amber", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/flashing-amber-" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.amber);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "amber");
});

test("andon flashing green", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/flashing-green-" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.green);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "green");
});

test("andon flashing blue", async t => {
	var frames = [];
	for(var i = 0; i<30;i++){
		var frame = await readImage("./testData/andon/flashing-blue-" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andonConfig.blue);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "blue");
});

const andon2Config = {
	green: {location:{x:"21%", y:"63%"}, radius:"9%"},
	blue: {location:{x:"31%", y:"20%"}, radius:"18%"},
	amber: {location: {x:"14%", y:"91%"}, radius:"9%"}
};

test("real world andon flashing green", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.green);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "green");
});

test("real world andon flashing blue", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.blue);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "blue");
});

test("real world andon flashing amber", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.amber);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "amber");
});

test("real world andon off green", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/1.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.green);

	t.is(result.metadata.state, "off");
});

test("real world andon off blue", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/1.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.blue);

	t.is(result.metadata.state, "off");
});

test("real world andon off amber", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/1.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.amber);

	t.is(result.metadata.state, "off");
});

test("real world andon on green", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/4.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.green);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "green");
});

test("real world andon on blue", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/4.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.blue);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "blue");
});

test("real world andon on amber", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon2/4.jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, andon2Config.amber);

	t.is(result.metadata.state, "solid");
	t.is(result.metadata.color, "amber");
});

test("real world andon flashing green with blue interference", async t => {
	var frames = [];
	for(var i = 1; i<=24;i++){
		var frame = await readImage("./testData/andon3/" + i + ".jpg");
		frames.push(frame);
	}

	var result = await transforms.andonState(frames, {location:{x:"21%", y:"63%"}, radius:"10%", vThreshold:.7});

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "green");
});

test("real world haas mill beacon light flashing amber (short)", async t => {
	var frames = [];
	for(var i = 1; i<=56;i++){
		var frame = await readImage("./testData/brief_amber_flashing/" + i + ".jpg");
		frames.push(frame);
	}

	var config = { location:{ "x":"50.0%", "y":"82.0%" }, radius:"16.0%", vThreshold:0.68};
	var result = await transforms.andonState(frames, config);

	t.is(result.metadata.state, "flashing");
	t.is(result.metadata.color, "amber");
});
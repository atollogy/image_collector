const cv = require("opencv");

function pctOrValue(value, max){
	if (typeof value == "string"){
		var parsedValue = (/%$/.test(value)) ? 
			(parseInt(value.substring(0,value.length-1)) / 100) * max : 
			parseInt(value)
		return (parsedValue > max) ? max : (parsedValue < 0) ? 0 : parsedValue;
	}
	return (value > max) ? max : (value < 0) ? 0 : value;
}
module.exports.pctOrValue = pctOrValue;

//debugging tool to read an image
module.exports.readImage = async function(path){
	return new Promise((resolve, reject) =>{
		cv.readImage(path, function(err, im){
			if(err){
				reject(err);
			} else {
				resolve(im);
			}
		});
	});
}

//debugging tool to read an image from a buffer
module.exports.readImageFromBuffer = async function(buffer){
	return new Promise((resolve, reject) =>{
		cv.readImage(buffer, function(err, im){
			if(err){
				reject(err);
			} else {
				resolve(im);
			}
		});
	});
}

//debugging tool to show an image
module.exports.show = async function(image){
	var window = new cv.NamedWindow('image', 0);
	window.show(image);
	window.blockingWaitKey(0, 50);
	return new Promise((resolve, reject) => {
		setTimeout(function(){
			// keep the windows open
			resolve();
		},5000);
	});
}

module.exports.rgbToHsl = function(r, g, b){
	var [red, green, blue] = [r,g,b].map(c => c/255);

	var maxChannel = Math.max(red, green, blue);
	var minChannel = Math.min(red, green, blue);

	var luminance = (minChannel + maxChannel) / 2;

	if(maxChannel == minChannel){
		return {h: 0, s: 0, l:luminance};
	}

	var saturation;
	if (luminance < 0.5){
		saturation = (maxChannel - minChannel)/(maxChannel + minChannel);
	} else {
		saturation = (maxChannel - minChannel)/(2.0 - maxChannel - minChannel)
	}

	var hue;
	if(red>=green && red>=blue){
		hue = (green - blue)/ (maxChannel - minChannel);
	} else if (green >= red && green >= blue){
		hue = 2.0 + (blue - red) / (maxChannel - minChannel);
	} else {
		hue = 4.0 + (red - green) / (maxChannel - minChannel);
	}

	return {h: hue * 60, s:saturation, l:luminance};
}

module.exports.rgbToHsv = function(r, g, b) {
  r /= 255, g /= 255, b /= 255;

  var max = Math.max(r, g, b), min = Math.min(r, g, b);
  var h, s, v = max;

  var d = max - min;
  s = max == 0 ? 0 : d / max;

  if (max == min) {
    h = 0; // achromatic
  } else {
    switch (max) {
      case r: h = (g - b) / d + (g < b ? 6 : 0); break;
      case g: h = (b - r) / d + 2; break;
      case b: h = (r - g) / d + 4; break;
    }
  }

  return { h: h * 60, s: s, v: v };
}

//progressively allow more variance as the white gets brighter
module.exports.getWhite = function (r, g, b){
	var achromatic = 192;
	var low = Math.min(r, g, b);
	var wiggle = 2 + (low - achromatic) / 4;
	return (low > achromatic) && (r - low < wiggle) && (g - low < wiggle) && (b - low < wiggle);
}

module.exports.lightState = function (isOn, isFlashing = false){
	return isFlashing ? "flashing" : isOn ? "solid" : "off";
}

module.exports.hueToColorName = function (hue, isWhite = false){
	if (isWhite){
		return "white";
	} else if (hue < 0 || hue > 360){
		return "";
	} else if (hue < 27 || hue >= 325){
		return "red";
	} else if (hue >= 27 && hue < 80){
		return "amber";
	} else if (hue >= 80 && hue < 175 ){
		return "green";
	} else if (hue >= 175 && hue < 270){
		return "blue";
	} else if (hue >= 270 && hue < 325){
		return "magenta";
	}
}


module.exports.imageSubDimensionsWithRadius = function(image, parameters){
	var width = image.width();
	var height = image.height();
	var radius = pctOrValue(parameters.radius || 10, Math.min(width, height));
	var x = Math.max(0, ((radius * 2) >= width) ? 0 : (pctOrValue(parameters.location.x, width) - radius));
	var y = Math.max(0, ((radius * 2) >= height) ? 0 : (pctOrValue(parameters.location.y, height) - radius));

	return {
		imageWidth:width,
		imageHeight:height,
		radius:radius,
		x:x,
		y:y,
		width: Math.min(radius*2, width),
		height: Math.min(radius*2, height)
	}
}

module.exports.annotateAndonImage = function(image, parameters, result){
	const boxColor = [255, 0, 0];
	const textColor = [255, 255, 255];
	const textBackgroundColor= [0,0,0];

	var dimensions = module.exports.imageSubDimensionsWithRadius(image, parameters);
	var x = dimensions.x;
	var y = dimensions.y;
	var w = dimensions.width;
	var h = dimensions.height;

	var videoWidth = (image.width() == 480) ? 640 : 320;
	if (image.width() != videoWidth){
		var scale = videoWidth/image.width();
		var videoHeight = Math.floor(scale*image.height());
		image.resize(videoWidth, videoHeight);
		x = Math.floor(scale*x);
		y = Math.floor(scale*y);
		w = Math.floor(scale*w);
		h = Math.floor(scale*h);
	}

	image.rectangle([x, y], [w, h], boxColor, 1);

	if (false) {
		var textY = (y > 15) ? ( y - 4) : (y + h + 12);
		var text = (result.state == 'off')? 'off' : (result.state +' '+result.color);
		var textWidth = 9*text.length;
		image.fillPoly([[[x, textY-11],[x+textWidth, textY-11],[x+textWidth,textY+3],[x, textY+3]]], textBackgroundColor);
		image.putText(text, x, textY, "HERSEY_SIMPLEX", textColor, 0.5, 1);
	}

	return image;
}